<?php  

global $config; 
global $base_url; 
global $language ;
$lang_name = $language->language ;

include_once 'api_helpers.php';

//Operating Countries
$config['mundio_countries'] = array("GB" => "United Kingdom", "BE" => "Belgium", "DK" => "Denmark", "SE" => "Sweden", "NL" => "Netherlands", "AT" => "Austria", "FR" => "France", "PT" => "Portugal");

//Freesim Site Codes
$config['freesim_sitecode'] = array("UK" => "MCM", "GB" => "MCM", "BE" => "MBE", "DK" => "BDK", "SE" => "BSE", "NL" => "BNL", "AT" => "BAU", "FR" => "MFR", "PT" => "MPT");

//Two in One SIM Countries
$config['two_in_one_sim_countries'] = array("AT" => "Austria", "FR" => "France", "NL" => "Netherlands", "SE" => "Sweden", "PT" => "Portugal", "UK" => "United Kingdom" );

$config['id_proofs'] = array("1" => "Passport", "2" => "ID Card", "3" => "Driving Licence", "4" => "Visa", "5" => "Birth Certificate" );

//Debug = 1 Email will be triggered to dev team
$config['debug'] = 1;
$config['website_email'] = 'noreply@vectonemobile.fr';

// Custom module configuration
$config['brand'] = 1; //For Vectone
$config['productCode'] = "WEB";
$config['paym_productCode'] = "PP";
$config['sitecode'] = "BAU";
$config['topup_sitecode'] = "BAU";
$config['send_a_sim_account_id'] = 'FREE SIM';
$config['send_a_sim_pay_ref_code'] = 'SIM';
$config['country_saver_pay_ref_code'] = 'CSB';
$config['topup_made_by'] = "VMAT-web-topup";
$config['Auto_Topup_MinLevelID'] = 1;
$config['channel_source'] = "VMAT Website";
$config['freesimstatus'] = 24;
$config['subscriberchannel'] = "NewOrder(3 in 1 SIM fits all)";
$config['CREDIT_SIM_subscriberchannel'] = "PaidSIM(3 in 1 SIM fits all)";
$config['ACTIVATION_SIM_subscriberchannel'] = "Activation(3 in 1 SIM fits all)";
$config['recomend_subscriber_channel'] = "RECOM_SEND_FRIEND_FREESIM";
$config['sourcereg'] = "VMAT Website";
$config['country'] = "Austria";
$config['input_country'] = "Austria";
$config['countrycode'] = "AUT";
$config['country_code']="AT";
$config['rates_countrycode'] = "VMAT";
$config['product'] = "VMAT";
$config['dialing_code'] = "43";
$config['lang_code'] = "en";
$config['currency_symbol'] = "&euro;";
$config['currency'] = "EUR";
$config['currencyCode'] = "&#8364;";

$config['paytype_paym']="PAYM";
$config['paytype_payg']="PAYG";
$config['paym_country_code'] = "GBR";
$config['paym_subscriberchannel'] = "POSTPAID";

$config['customer_service_free_number'] = "2211";
$config['customer_service_alternate_number'] = "0688 911 68 88";
$config['paym_llom_paymode'] = 2;
$config['paym_llom_svcid'] = 5;
$config['post_code_length'] = 4;
$config['debug_send_mail_id'] = 'm.arunkumar@mundio.com';

// Bundle ID
$config['national_bundle_id'] = 21;
$config['data_bundle_id'] = 22;
$config['saver_bundle_id'] = 23;

//Rates Module API
define("VM_INTERNATIONAL_RATES", "/Getstandardsmartrates");
define("VM_MOBILE_OPERATOR", "http://192.168.2.102:9706/api/v1/OperatorList");
define("VM_CHEAPCALL_RATES", "/Dmcheapcallratesgetsp");
define("VM_UKRATES", "http://192.168.2.102:9706/api/v1/UKRates");
//define("VM_ROAMINGRATES", "http://192.168.2.102:9706/api/v1/RoamingRates");
define("VM_ROAMINGRATES", "/Websitecentralgetroamingratesinfo");
define("VM_SMART_ROAMINGRATES", "http://192.168.2.102:9706/api/v1/SmartRoamingRates");
define("VM_NATIONAL_RATES", "/Websitecentralratenational");
define("VM_VECTONEAPP_RATES", "/Websitecentralvectoneapprates");
define("VM_VECTONEAPP_NATIONAL_RATES", "/Websitecentralvectoneappnational");

//Bundles
define("VM_BUNDLES_CATEGORIES", "/Websitecentralviewbundlecategory");
define("VM_BUNDLES_LIST", "/Websitecentralviewbundleinfo");
define("VM_BUNDLES_SPECIAL_COUNTRY_PRICE", "/Websitecentralgetspecialcountry");

//VM Features - Operators list by Country
define("VM_COUNTRIES_BY_CODE", "/Websitecentralgetcountrynamecode");
define("VM_OPERATORS_BY_CCODE", "/Websitecentralgetoperatorname");

//VM Features - Mobile Internet
define("VM_HANDSET_MANUFACTURERS", "/Websitecentralgetmobilenamecode");
define("VM_MOBILE_MODELS", "/Websitecentralgetmobilemodelcode");
define("VM_MOBILE_MODEL_DISCURSION", "/Websitecentralgetmobilediscursion");
define("VM_MOBILE_DATA_ACTIVATION", "/Dataactivation");

//Myaccount login url
$config['myaccount_login_url'] = "http://stagingmyaccount.vectonemobile.fr";

// MyAccount module
define('API_HOST', 'http://10.22.4.56:1018/v1/user/');
define('VM_MYACCOUNT_LOGIN_PWD_ENCRYPT', 'http://192.168.2.102:9038/v1/user/8ak37gJUYq2hRz/Encrypt');
define('VM_MYACCOUNT_FORGOT_PASSWORD_API', '/Websitecentralforgotpasswordbymobile');
define('VM_MYACCOUNT_FORGOT_SET_TOKEN_EXPIRY', '/Websitecentralforgotpasswdexpcountinsert');
define('VM_MYACCOUNT_FORGOT_CHECK_TOKEN_EXPIRY', '/Websitecentralforgotpasswdcheckexpirycount');
define('VM_MYACCOUNT_RESET_PASSWORD', '/Websitecentralresetpassword');
define('VM_MYACCOUNT_LOGIN_CHECK_API', '/Signin');
define('VM_REGISTERED_AND_BRAND_VERIFY_API', '/Websitecentralregistercheckmsisdn');
define('VM_MYACCOUNT_SEND_PIN_API', '/Sendpin'); 
define('VM_MYACCOUNT_REGISTER', '/Websitecentralregisterpersonalinfo');

// Freesim order
define('VM_FREESIM_ORDER', '/Websitecentralfreesimorder');
define('VM_FREESIM_ORDER_REFERRER', '/Websitecentralreferralordernewsim');
//Free SIM with Credit
define('VM_FREESIM_WITH_CREDIT_ORDER', '/websitecentralcreatesimwithcredit');
define('VM_FREESIM_WITH_CREDIT_3DS_LOG', '/Websitecentralthreedspaymentlogin');
//Send a SIM Abroad
define('VM_SEND_A_SIM_ABROAD_DELIVERY_COST', '/Websitecentralsimordergetprice');
define('VM_SEND_A_SIM_ABROAD_CHECK_DELIVERY_ADDRESS', '/Websitecentralsimordercheckaddress');
define('VM_SEND_A_SIM_ABROAD_ORDER', '/Websitecentralsimordersubsidcreate');
define('VM_SEND_A_SIM_ABROAD_ORDER_CRM', '/Websitecentralsendsimabroadfreesimcreate');
//Two in One Sim - Get method URL webservice
define('VM_TWO_IN_ONE_SIM_ORDER', 'http://192.168.41.23:1830/api/Customer2in1/');

//Topup
define('VM_CHECK_ACTIVE_NUMBER', '/Websitecentraltopupcheckactivemobileno');
define('VM_TOPUP_DENOMINATIONS', '/Webtopupamountspmyaccountdm');
define('VM_RX_PAYMENT_STEP1', '/CTACCTopupStep1');
define('VM_RX_PAYMENT_STEP2', '/CTACCTopupStep2');
define('VM_TOPUP_ADDCREDIT', '/Websitecentraltopupprocessbyccdcwithsmsv2');
define('VM_TOPUP_PAYMENT_LOG', '/Websitecentraltopuppaymentloginsert');
define('VM_AUTO_TOPUP', '/Websitecentralenableautotopup');
define('VM_AUTO_TOPUP_LOG', '/Websitecentralautotopuplog');
define('VM_FAILURE_PAYMENT_UPDATE', '/Websitecentraltopupinsertpaymentfailedtransaction');
define('VM_TOPUP_GET_BALANCE', '/Websitecentraltopupgettopuplogdetails');
define('VM_TOPUP_STATUS_SMS', '/SendTopupStatusSMS');

//Auto Topup API
define('DMNL_MYACCOUNT_ADD_NEW_ORDER', 'http://192.168.2.102:9706/api/v1/AddNewOrder');
define('DMNL_MYACCOUNT_PAY_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/PayAutoTopup');

define('DMNL_MYACCOUNT_DELETE_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/DeleteAutoTopup');
 
//Refer a Friend Bonus Topup API
define('VM_RERERAFRIEND_BONUS_TOPUP', '/Websitecentralrfrreferraltopup');

// PaySafe Implemenation API
define('PAYSAFE_API_KEY' , 'http://192.168.2.102:9810/api/v1/Key');
define('PAYSAFE_CARDURL' , 'http://192.168.2.102:9810/api/v1/PaySafe');
define('PAYSAFE_INFO', 'http://192.168.2.102:9810/api/v1/PaySafeInfo');
define('PAYSAFE_GETSERIAL' , 'http://192.168.2.102:9810/api/v1/PaySafeGetSerial');
define('PAYSAFE_EXECUTEDEBIT' , 'http://192.168.2.102:9810/api/v1/PaySafeExecuteDebit');
define('PAYSAFE_CANCEL' , 'http://192.168.2.102:9810/api/v1/PaySafeCancel');

define('PAYSAFE_CURRENCY_EXCHANGE' ,  'http://192.168.2.102:9810/api/v1/InsertCurrencyExchange');
define('PAYSAFE_CURRENCY_UPDATE' , 'http://192.168.2.102:9810/api/v1/CurrencyUpdate');
define('PAYSAFE_GET_ERROR_CODE', 'http://192.168.2.102:9810/api/v1/ErrorMsgGet');
define('PAYSAFE_LOG_INSERT', 'http://192.168.2.102:9810/api/v1/PaymentLogInsert');
define('PAYSAFE_INSERT_MER_REF_DETAILS', 'http://192.168.2.102:9810/api/v1/InsertMerchantRefDtls');
define('PAYSAFE_INSERT_PNURL_LOG', 'http://192.168.2.102:9810/api/v1/InsertPnurlLog');
define('PAYSAFE_GET_PNRUL_LOG', 'http://192.168.2.102:9810/api/v1/GetPnurlLog');

//LLOM APIs
define('LLOM_COUNTRY_LIST', '/Websitecentralllomcountrylist');
define('LLOM_PLAN_RATE_LIST', '/Websitecentralllomrates');
define('LLOM_AREA_LIST', '/Websitecentralllomgetarealist');
define('LLOM_AREA_CODE_LIST', '/Websitecentralllomgetareacode');
define('LLOM_NUMBER_LIST', '/Websitecentralllomgetavailablenumber');
define('LLOM_RELEASE_NUMBER', '/Websitecentralllomreleaseavailableno');
define('LLOM_CHECK_SUBSCRIPTION', '/Websitecentralcheckmultinbsubscribtion');
define('LLOM_SUBSCRIBE', '/Websitecentralllomsubscribe');

//PayMonthly
 define('VM_PAYM_PLAN_LIST', '/Websitecentralpaymonthlygetplan');
  
 define('VM_DD_GOCARD_NEWCUS_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardnewcustomer');
 define('VM_DD_GOCARD_NEWBANK_ACC_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardnewbankAccount');
 define('VM_DD_GOCARD_NEWMANDATE_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardNewMandate');
 
 define('VM_INSERT_DD_ORDER', '/Websitecentralpaymonthlyinsertdirectdebitorder');
 define('VM_PAYM_ACTIVITY_LOG', '/Websitecentralpaymonthlywritelog');
 define('VM_INSERT_NEW_SUBSCRIBER', '/Websitecentralpaymonthlyinsertnewsubscriber');
 define('VM_UPDATE_EXISTING_SUBSCRIBER', '/Websitecentralpaymonthlyupdateexistsubscriber');
 define('VM_INSERT_SIM_ORDER', '/Websitecentralpaymonthlyfreesimcreate');
 define('VM_GET_BUNDLEID', '/Websitecentralpaymonthlygetitembyitemid');
 
 
 define('VM_DD_DETAILS_INSERT_CRM', '/Websitecentralpaymonthlyinsertddaccount');
 define('VM_PAYG_TO_PAYM_SUBSCRIBE', '/Websitecentralpostpaidsubscribe'); 
 
 define('VM_PAYM_NEW_ORDER_DETAIL', '/Websitecentralppaddneworderdetail'); 


//Country Saver
$config['country_saver_planamount'] = array('amount' => '10', 'call' => '1000');
$config['country_saver_plancalltxt'] = array('call' => 'min from landline');
$config['country_saver_planamountor'] = array('call' => '400');
$config['country_saver_planamountortxt'] = array('call' => 'min from mobile');
$config['country_saver_addextra'] = array('amount' => '10','call' => '500');
$config['country_saver_addcallextra'] = array('call' => 'min from landline');
$config['country_saver_addextraor'] = array('call' => '500');
$config['country_saver_addextraortxt'] = array('call' => 'min from mobile');
$config['country_saver_amount'] = 10;
$config['country_saver_minutes'] = 1000;
$config['country_saver_addextraamount'] = 10;
$config['country_saver_addextraminutes'] = 500;
$config['sri_lanka_country_code'] = '94';
$config['country-saver-cms-link'] = '/country-saver';
$config['country_saver_add_ons_text'] = "Add extra 500 minutes to for €10";
$config['country_saver_payment_method_option'] = array('' =>'Select', '1'=>'Pay by credit', '2' => 'Pay by card');
$config['credit_card_expiry_month'] = array(
	    "" => "Select",
        "01" => "Jan",
        "02" => "Feb",
        "03" => "Mar",
        "04" => "Apr",
        "05" => "May",
        "06" => "Jun",
        "07" => "Jul",
        "08" => "Aug",
        "09" => "Sep",
        "10" => "Oct",
        "11" => "Nov",
        "12" => "Dec"
    );
define('VM_CS_GET_REGINFO', '/Websitecentralfsbgetregistrationinfo'); 	
define('VM_CS_CHECK_MOB_BAL', '/Websitecentralfsbcheckbalance'); 	
define('VM_CS_CHECK_SUBSCRIBE', '/Websitecentralfsbcheckmsisdn'); 	
define('VM_CS_INSERT_REGISTRATION', '/Websitecentralfsbinsertregistration'); 	
define('VM_CS_UPDATE_REGISTRATION', '/Websitecentralfsbupdateregistration'); 	
define('VM_CS_UPDATE_REGISTRATION_PAYWITH_CC', '/Websitecentralfsbupdateregistrationpaywithcreditcard'); 
define('VM_CS_INSERT_PAYMENT_FAILED_TRANSACTION', '/Websitecentralinsertpaymentfailedtransaction'); 	
define('VM_CS_CHECK_MSISDN_ADDONSINFO', '/Websitecentralfsbcheckmsisdnaddonsinfo'); 
define('VM_CS_UPDATE_REGISTRATION_PAYWITH_CCFAILED', '/Websitecentralfsbupdateregistrationpaywithcreditcardfailed'); 	
define('VM_CS_ADDON_REGISTRATION', '/Websitecentralfsbaddonregistration'); 	
define('VM_CS_INSERT_ACTIVITY_LOG', '/Websitecentralfsbinsertactivitylog'); 	


//SIM Activation feature for VMFR
define('VM_SIM_ACTIVATION_CHECK', '/Websitecentralactivationsimregistercheck');
define('VM_SIM_ACTIVATION_CUSTOMER_ADD', '/Websitecentralactivationinsertnewsusbscriber');
define('VM_SIM_ACTIVATION', '/Websitecentralactivationinsertfreesimregistered');
	
//Site urls
if($lang_name=="en")
{
	$config['contact_url'] = "/en/contact-us";
	$config['tc_url'] = "/en/terms-and-conditions";
	$config['pp_url'] = "/en/privacy-policy";
	$config['getting_started_url'] = "/en/getting-started";
	$config['bundles_url'] = "/en/national-bundles";
	$config['rates_url'] = "/en/exclusive-low-rates";
	$config['vectone_app_url'] = "/en/vectone-app";
	$config['llom_url'] = "/en/landline-mobile";
	$config['myaccountregisterurl'] = "/en/myaccount/register/step1";
	$config['myaccountloginurl'] = "/en/myaccount/login";	
	$config['paym_palnpageurl'] = "/en/paym/plans";	
}	
else 
{
	$config['contact_url'] = "/nous-contacter";
	$config['tc_url'] = "/conditions-générales";
	$config['pp_url'] = "/politique-de-confidentialité";
	$config['getting_started_url'] = "/getting-started";
	$config['bundles_url'] = "/forfaits-nationaux";
	$config['rates_url'] = "/tarifs-internationaux";
	$config['vectone_app_url'] = "/vectone-app";
	$config['llom_url'] = "/fixe-sur-mobile";
	$config['myaccountregisterurl'] = "/myaccount/register/step1";
	$config['myaccountloginurl'] = "/myaccount/login";	
	$config['paym_palnpageurl'] = "/paym/plans";
}	

	
//Banner title for custom module
if($lang_name=="en")
{
	
	if(arg(0)== "topup"){
		$config['banner_title1'] = "Top up online";
		$config['banner_title2'] = "Top up online from the comfort of your couch or your kitchen worktop!";
		$config['banner_title3'] = "In order to top up online, all you need is your debit/credit card at reach if you’re in the National or a Paysafecard voucher if you’re abroad. Additionally, you can even enable auto top up and ensure that you’re never low on balance again! ";
	}
		
	else if(arg(0)== "freesim" || arg(1)== "freesim" || arg(0)== "simorder"){
		$config['banner_title1'] = "Free SIM";
		$config['banner_title2'] = "Choose your Top-up amount.";
		$config['banner_title3'] = "Now you can Top Up by SMS, if you have Topped Up online with Vectone before. You will be billed on the same card as the one you used for your last transaction. ";		
	}
		
	else if(arg(0)== "myaccount"){
		$config['banner_title1'] = "My Vectone";
		$config['banner_title2'] = "Welcome to My Account! With a registered My Account, you are ensured data security and independence in managing your account";
		$config['banner_title3'] = "Pay your bills, view your calling history, refer a friend or manage your top up easily, all at one place ";
	}	

	else if(arg(1)== "sendasim"){
		$config['banner_title1'] = "Send a SIM abroad";
		$config['banner_title2'] = "With Vectone operating in many countries around Europe you can send a SIM to loved one";
		$config['banner_title3'] = "From great bundles, to ultra cheap international rates and free Vectone to Vectone calls we offer a wide range of ways to save money. Now you can send a SIM to someone in a Vectone country and help them to stay in touch. ";
	}
	
	else if(arg(0)== "llom"){
		$config['banner_title1'] = "Landline on Mobile";
		$config['banner_title2'] = "Get a National or international landline number that redirects calls to your mobile phone";
		$config['banner_title3'] = "This feature allows business users to appear professional and local, and allows family users to have a local standard rate number that their friends can use to cheaply stay in touch";
	}
	
	else if(arg(0)== "paym"){
		$config['banner_title1'] = "Pay Monthly Plans";
		$config['banner_title2'] = "Always stay touch with our Pay Monthly Plans with Super Allowances";
		$config['banner_title3'] = "Our Pay Monthly plans are the best ways to combine value and convenience without long term commitment. With our one-month rolling contracts you are never locked into a plan that doesn't suit you.";
	}
	else if(arg(0)== "countrysaver"){
		$config['banner_title1'] = "Country Saver";
		$config['banner_title2'] = "Save money calling overseas with a convenient package of minutes";
		$config['banner_title3'] = "With Country Saver, you can relax knowing that you are saving money with an economical package of minutes to call your chosen country.";
	}
		
}
else	
{
	if(arg(0)== "topup"){
		$config['banner_title1'] = "Rechargement en Ligne";
		$config['banner_title2'] = "Rechargez en ligne depuis le confort de votre canapé ou de votre plan de travail de cuisine!";
		$config['banner_title3'] = "Afin de recharger en ligne, tout ce dont vous avez besoin est votre carte de débit/crédit si vous êtes dans le Pays concerné ou un bon Paysafecard si vous êtes à l'étranger. De plus, vous pouvez même activer le rechargement automatique et vous assurer de ne plus jamais avoir un solde nul! ";
	}
		
	else if(arg(0)== "freesim" || arg(1)== "freesim" || arg(0)== "simorder"){
		$config['banner_title1'] = "Carte SIM Gratuite";
		$config['banner_title2'] = "Choisissez le montant du Rechargement";
		$config['banner_title3'] = "Dorénavant vous pouvez Recharger par SMS, si vous avez Rechargé en ligne auparavant avec Vectone. Vous serez facturé sur la même carte utilisée lors de votre dernière transaction.";		
	}
		
	else if(arg(0)== "myaccount"){
		$config['banner_title1'] = "Mon Vectone";
		$config['banner_title2'] = "Bienvenue chez Mon Compte! Avec Mon Compte enregistré, vous êtes assurés de la sécurité de vos données et de l'indépendance pour gérer votre compte";
		$config['banner_title3'] = "Payez vos factures, regardez l'historique de vos appels, parrainez un ami ou gérez votre rechargement facilement, tout en un seul endroit";
	}	
	
	else if(arg(1)== "sendasim"){
		$config['banner_title1'] = "Envoyez une carte SIM à l'étranger";
		$config['banner_title2'] = "Avec Vectone opérant dans de nombreux pays à travers l'Europe, vous pouvez envoyer une carte SIM à ceux que vous aimez";
		$config['banner_title3'] = "De bons forfaits à des tarifs extrêmement bas et des appels gratuits Vectone à Vectone, nous offrons une large gamme de moyens d'économiser de l'argent. Dorénavant vous pouvez envoyer une carte SIM à quelqu'un localisé dans un pays Vectone et les aider à garder contact.";
	}
	
	else if(arg(0)== "llom"){
		$config['banner_title1'] = "Fixe sur Mobile";
		$config['banner_title2'] = "Obtenez un numéro fixe national ou International qui redirige les appels vers votre téléphone portable";
		$config['banner_title3'] = "Cette fonctionnalité permet aux professionels d'apparaître locaux et permet aux familles d'avoir un numéro à tarif local que leurs amis peuvent utiliser pour rester en contact à faibles coûts";
	}
	
	else if(arg(0)== "paym"){
		$config['banner_title1'] = "Pay Monthly Plans";
		$config['banner_title2'] = "Always stay touch with our Pay Monthly Plans with Super Allowances";
		$config['banner_title3'] = "Our Pay Monthly plans are the best ways to combine value and convenience without long term commitment. With our one-month rolling contracts you are never locked into a plan that doesn't suit you.";
	}
	else if(arg(0)== "countrysaver"){
		$config['banner_title1'] = "Country Saver";
		$config['banner_title2'] = "Save money calling overseas with a convenient package of minutes";
		$config['banner_title3'] = "With Country Saver, you can relax knowing that you are saving money with an economical package of minutes to call your chosen country.";
	}
		
}
	
	
$config['ccode'] = array('Afghanistan'=>'AFG',
'Albania'=>'ALB',
'Algeria'=>'DZA',
'AmericanSamoa'=>'ASM',
'American Samoa'=>'ASM',
'Andorra'=>'AND',	
'Angola'=>'AGO',
'Anguilla'=>'AIA',
'Antarctica'=>'ATA',
'Antigua Barbuda'=>'ATG',
'Argentina'=>'ARG',
'Armenia'=>'ARM',
'Aruba'=>'ABW',
'Ascension'=>'ASC',
'Australia'=>'AUS',
'Austria'=>'AUT',
'Azerbaijan'=>'AZE',
'Bahamas'=>'BHS',
'Bahrain'=>'BHR',
'Bangladesh'=>'BGD',
'Barbados'=>'BRB',
'Belarus'=>'BLR',
'Belgium'=>'BEL',
'Belize'=>'BLZ',
'Benin'=>'BEN',
'Bermuda'=>'BMU',
'Bhutan'=>'BTN',
'Bolivia'=>'BOL',
'Bosniaherzegovina'=>'BIH',
'Bosnia Herzegovina'=>'BIH',
'Botswana'=>'BWA',
'Brazil'=>'BRA',
'British Virgin Islands'=>'VGB',
'Bulgaria'=>'BGR',
'Burkina Faso'=>'BFA',
'Burundi'=>'BDI',
'Cambodia'=>'KHM',
'Cameroon'=>'CMR',
'Canada'=>'CAN',
'CanaryIslands'=>'ICT',
'Cape Verde'=>'CPV',
'Cayman Islands'=>'CYM',
'Central African Republic'=>'CAF',
'Chad'=>'TCD',
'Chattham Islands'=>'CMI',
'Chile'=>'CHL',
'China'=>'CHN',
'Christmas Islands'=>'CXR',
'CocosIslands'=>'CCK',
'Colombia'=>'COL',
'Comoros'=>'COM',
'Congo'=>'COG',
'Congo CDR'=>'COD',
'Cook Islands'=>'COK',
'CostaRica'=>'CRI',
'Croatia'=>'HRV',
'Cuba'=>'CUB',
'Cyprus'=>'CYP',
'Cyprus North'=>'CYN',
'Czech Republic'=>'CZE',
'Czechrepublic'=>'CZE',
'Denmark'=>'DNK',
'Diego Garcia'=>'DGA',
'Djibouti'=>'DJI',
'Dominica'=>'DMA',
'Dominican Republic'=>'DOM',
'East Timor'=>'TMP',
'Ecuador'=>'ECU',
'Egypt'=>'EGY',
'ElSalvador'=>'SLV',
'Equatorial Guinea'=>'GNQ',
'Eritrea'=>'ERI',
'Estonia'=>'EST',
'Ethiopia'=>'ETH',
'Falkland Islands'=>'FLK',
'Faroe Islands'=>'FRO',
'Fiji'=>'FJI',
'Finland'=>'FIN',
'France'=>'FRA',
'French Guiana'=>'GUF',
'French Polynesia'=>'PYF',
'Gabon'=>'GAB',
'Gambia'=>'GMB',
'Georgia'=>'GEO',
'Germany'=>'DEU',
'Ghana'=>'GHA',
'Gibraltar'=>'GIB',
'Greece'=>'GRC',
'Greenland'=>'GRL',
'Grenada'=>'GRD',
'Guadeloupe'=>'GLP',
'Guam'=>'GUM',
'Guantanamo'=>'GTO',
'Guatemala'=>'GTM',
'Guinea Bissau'=>'GNB',
'Guinea Republic'=>'GIN',
'Guyana'=>'GUY',
'Haiti'=>'HTI',
'Honduras'=>'HND',
'Hongkong'=>'HKG',
'Hungary'=>'HUN',
'Iceland'=>'ISL',
'India'=>'IND',
'Indonesia'=>'IDN',
'Iran'=>'IRN',
'Iraq'=>'IRQ',
'Ireland'=>'IRL',
'Israel'=>'ISR',
'Italy'=>'ITA',
'IvoryCoast'=>'CIV',
'Jamaica'=>'JAM',
'Japan'=>'JPN',
'Jordan'=>'JOR',
'Kazakhstan'=>'KAZ',
'Kenya'=>'KEN',
'Kiribati'=>'KIR',
'NorthKorea'=>'PRK',
'KoreaSouth'=>'KOR',
'SouthKorea'=>'KOR',
'Kosovo'=>'UNK',
'Kuwait'=>'KWT',
'Kyrgyzstan'=>'KGZ',
'Laos'=>'LAO',
'Latvia'=>'LVA',
'Lebanon'=>'LBN',
'Lesotho'=>'LSO',
'Liberia'=>'LBR',
'Libya'=>'LBY',
'Liechtenstein'=>'LIE',
'Lithuania'=>'LTU',
'Luxembourg'=>'LUX',
'Macau'=>'MAC',
'Macedonia'=>'MKD',
'Madagascar'=>'MDG',
'Malawi'=>'MWI',
'Malaysia'=>'MYS',
'Maldives'=>'MDV',
'Mali'=>'MLI',
'Malta'=>'MLT',
'Mariana Islands'=>'TIQ',
'Marshall Islands'=>'MHL',
'Martinique'=>'MTQ',
'Mauritania'=>'MRT',
'Mauritius'=>'MUS',
'Mayotte'=>'MYT',
'Mexico'=>'MEX',
'Micronesia'=>'FSM',
'Moldova'=>'MDA',
'Monaco'=>'MCO',
'Mongolia'=>'MNG',
'Montenegro'=>'MNE',
'Montserrat'=>'MSR',
'Morocco'=>'MAR',
'Mozambique'=>'MOZ',
'Myanmar Burma'=>'MMR',
'Namibia'=>'NAM',
'Nauru'=>'NRU',
'Negara Brunei'=>'BRU',
'Nepal'=>'NPL',
'Netherlands'=>'NLD',
'Netherlands Antilles'=>'ANT',
'New Caledonia'=>'NCL',
'New Zealand'=>'NZL',
'Nicaragua'=>'NIC',
'Niger'=>'NER',
'Nigeria'=>'NGA',
'Niue'=>'NIU',
'Norfolk Island'=>'NFK',
'Norway'=>'NOR',
'Oman'=>'OMN',
'Pakistan'=>'PAK',
'Palau'=>'PLW',
'Palestine'=>'PSE',
'Panama'=>'PAN',
'Papua New Guinea'=>'PNG',
'Paraguay'=>'PRY',
'Peru'=>'PER',
'Philippines'=>'PHL',
'Poland'=>'POL',
'Portugal'=>'PRT',
'Puerto Rico'=>'PRI',
'Qatar'=>'QAT',
'Reunion'=>'REU',
'Romania'=>'ROU',
'Russia'=>'RUS',
'Rwanda'=>'RWA',
'Saipan Northern Marianas'=>'MNP',
'San Marino'=>'SMR',
'Sao Tome Principe'=>'STP',
'Saudi Arabia'=>'SAU',
'Senegal'=>'SEN',
'Senegal Sentel'=>'SNT',
'Serbia'=>'SRB',
'Seychelles'=>'SYC',
'SierraLeone'=>'SLE',
'Singapore'=>'SGP',
'Slovakia'=>'SVK',
'Slovenia'=>'SVN',
'Slovenia IPKO'=>'SVI',
'Solomon Islands'=>'SLB',
'Somalia'=>'SOM',
'South Sudan'=>'SSD',
'South Africa'=>'ZAF',
'Southafrica'=>'ZAF',
'Spain'=>'ESP',
'SriLanka'=>'LKA',
'Sri Lanka'=>'LKA',
'Srilanka'=>'LKA',
'St Helena'=>'SHN',
'St Kitts Nevis'=>'KNA',
'St Lucia'=>'LCA',
'St Martin'=>'SMR',
'St Pierre Miquelon'=>'SPM',
'St Vincent Grenadines'=>'VCT',
'Sudan'=>'SDN',
'Suriname'=>'SUR',
'Swaziland'=>'SWZ',
'Sweden'=>'SWE',
'Switzerland'=>'CHE',
'Syria'=>'SYR',
'Taiwan'=>'TWN',
'Tajikistan'=>'TJK',
'Tanzania'=>'TZA',
'Thailand'=>'THA',
'Thuraya'=>'TRA',
'Togo'=>'TGO',
'Tokelau'=>'TKL',
'Tonga'=>'TON',
'Trinidad and Tobago'=>'TTO',
'Tunisia'=>'TUN',
'Turkey'=>'TUR',
'Turkmenistan'=>'TKM',
'Turks Caicos'=>'TCA',
'Tuvalu'=>'TUV',
'Uganda'=>'UGA',
'Ukraine'=>'UKR',
'UnitedArabEmirates'=>'ARE',
'United Arab Emirates'=>'ARE',
'UnitedStates'=>'USA',
'United States'=>'USA',
'Uruguay'=>'URY',
'US Virgin Islands'=>'VIR',
'Uzbekistan'=>'UZB',
'Vanuatu'=>'VUT',
'Vatican City'=>'VAT',
'Venezuela'=>'VEN',
'Vietnam'=>'VNM',
'Wallis Futuna'=>'WLF',
'Western Samoa'=>'WSM',
'Yemen'=>'YEM',
'Zambia'=>'ZMB',
'Zimbabwe'=>'ZWE',
'Usa'=>'USA',
'USA'=>'USA',
'Uk'=>'GBR',
'United Kingdom'=>'GBR',
'UnitedKingdom'=>'GBR'

);


if($lang_name=="fr"){

	$config['trans_country'] = array(
	'Bangladesh'=>'Bangladesh',
	'Brazil'=>'Brésil',
	'Bulgaria'=>'Bulgarie',
	'France'=>'France',
	'Germany'=>'Allemagne',
	'Greece'=>'Grèce',	
	'India'=>'Inde',
	'Italy'=>'Italie',
	'Morocco'=>'Maroc',
	'Netherlands'=>'Pays-Bas',
	'Nigeria'=>'Nigéria',
	'Pakistan'=>'Pakistan',
	'Poland'=>'Pologne',
	'Portugal'=>'Portugal',
	'Romania'=>'Roumanie',
	'Czech Republic'=>'République Tchèque',
	'UK'=>'Royaume-Uni',
	'USA'=>'États-Unis',
	'Turkey'=>'Turquie',
	);
}

if($lang_name=="nl"){
	
		$config['trans_country'] = array(
		'Bangladesh'=>'Bangladesh',
		'Brazil'=>'Brazilië',
		'Bulgaria'=>'Bulgarije',
		'France'=>'Frankrijk',
		'Germany'=>'Duitsland',
		'Greece'=>'Griekenland',	
		'India'=>'India',
		'Italy'=>'Italië',
		'Morocco'=>'Marokko',
		'Netherlands'=>'Nederland',
		'Nigeria'=>'Nigeria',
		'Pakistan'=>'Pakistan',
		'Poland'=>'Polen',
		'Portugal'=>'Portugal',
		'Romania'=>'Roemenië',
		'Czech Republic'=>'Tsjechische Republiek',
		'UK'=>'Verenigd Koningkrijk',
		'USA'=>'Verenigde Staten',
		'Turkey'=>'Turkije',
	);
}

