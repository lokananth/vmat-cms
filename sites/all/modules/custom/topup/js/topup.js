(function ($) {
$(document).ready(function(){
	/* BEGIN: Step 1 Validation */

	$("#topup-registration-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
    $("#topup-registration-form #mobile-number, #topup-registration-form #confirm-mobile-number").on('blur' , function(){ 
        if($.trim($("#mobile-number").val()) != '' && $.trim($("#confirm-mobile-number").val()) != ''){
            $('#divTCMobile').remove();
			var mobile = $('#mobile-number').val();
			var mobile1 = $('#confirm-mobile-number').val();
			if( mobile != mobile1 ) {
				$('#divTCMobile').remove();
				$('#confirm-mobile-number').after('<div id="divTCMobile" class="errormsg">'+ Drupal.t("Mobile Number and Confirm Mobile Number did not match.") + '</div>');
			}
        }
    });
 
	/* END: Step 1 Validation */
	
	/* BEGIN: CC Step 2 Validation */
	
	$("#topup-payment-form input").on('keyup' , function() {
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
	$("#topup-payment-form select").change(function() {
		$(this).parent().parent().parent().next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});	
	/* END: CC Step 2 Validation */
	
	/* BEGIN: Paysafe Step 2 Validation */
	
	$("#topup-paysafe-step2-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
/*	$("#topup-payment-form #cardholder_name").on('blur' , function() { 
		$('#divCName').remove();
		if($.trim($(this).val()) == ''){
			$('#cardholder_name').after('<div id="divCName" class="errormsg">'+ Drupal.t('Please enter a card holder name.') + '</div>');
		}
	});	
	$("#topup-payment-form #card_number").on('blur' , function() { 
		$('#divCNo').remove();
		if($.trim($(this).val()) == ''){
			$('#card_number').after('<div id="divCNo" class="errormsg">'+ Drupal.t('Please enter a card number.') + '</div>');
		}
	});	
	$("#topup-payment-form #edit-expiry-title-month, #topup-payment-form #edit-expiry-title-year").on('change' , function() { 
		$('#divExpDate').remove();
		if($.trim($(this).val()) == ''){
			$('.form-type-date-select').after('<div id="divExpDate" class="errormsg">'+ Drupal.t('Please select Expiry Month & Year.') + '</div>');
		}
	});	
	$("#topup-payment-form #security_code").on('blur' , function() { 
		$('#divCVV').remove();
		if($.trim($(this).val()) == ''){
			$('#security_code').after('<div id="divCVV" class="errormsg">'+ Drupal.t('Please enter a security code.') + '</div>');
		}
	});	
	$("#topup-payment-form #address1").on('blur' , function() { 
		$('#divAdr1').remove();
		if($.trim($(this).val()) == ''){
			$('#address1').after('<div id="divAdr1" class="errormsg">'+ Drupal.t('Please enter the house number.') + '</div>');
		}
	});	
	$("#topup-payment-form #address2").on('blur' , function() { 
		$('#divAdr2').remove();
		if($.trim($(this).val()) == ''){
			$('#address2').after('<div id="divAdr2" class="errormsg">'+ Drupal.t('Please enter the street name.') + '</div>');
		}
	});	
	$("#topup-payment-form #town").on('blur' , function() { 
		$('#divAdr3').remove();
		if($.trim($(this).val()) == ''){
			$('#town').after('<div id="divAdr3" class="errormsg">'+ Drupal.t('Please enter the town.') + '</div>');
		}
	});	
	$("#topup-payment-form #pin_code").on('blur' , function() { 
		$('#divAdr4').remove();
		if($.trim($(this).val()) == ''){
			$('#pin_code').after('<div id="divAdr4" class="errormsg">'+ Drupal.t('Please enter the postcode.') + '</div>');
		}
	});	*/
	/* END: Paysafe Step 2 Validation */	

	//Validate Mobile Number STEP1
    $("#topup-registration-form #mobile-number").on('blur' , function(){ 
		$('#divTMobile').remove();
        if($.trim($(this).val()) != ''){            
            $.ajax({
                url: Drupal.settings.basePath + 'topup/mobile_validation',
                type: "POST",
                data:{
                    mobile_number: $(this).val()
                },
                success: function(data){    
                    if(data == -4){ //Mobile number is InActive                    
                        $('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Your Sim Is NOT ACTIVE. Please Contact Customer Service.') + '</div>');
                    }else if(data == -3){ //Mobile brand is not valid
                        $('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Please try with Vectone Mobile Number.') + '</div>');
                    }else if(data == -2){ //PAYM number is not allowing for Topup.
                        $('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('The PAYM Number is not allowed for Topup Process.') + '</div>');
                    }else if(data == -1){ //Mobile number not found.
                        $('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Please enter the valid mobile number and try again.') + '</div>');
                    }else{
                        $('#divTMobile').remove();
                    }
                }
            });
        }
    });
	
	
});
})(jQuery);