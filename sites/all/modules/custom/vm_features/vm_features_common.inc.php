<?php

/**
 * Country List 
 * @return type
 */
function get_dst_country_list() {
	global $config;
	$countryOptions = array();
	$param = array(
		"projectCode" => $config['product'],
		"languageCode" => 'ENG',
		"sitecode" => $config['sitecode'],
		"brand" => $config['brand']
	);	
	$response = apiPostNew(VM_COUNTRIES_BY_CODE, $param);
	//echo '<pre> URL : '.VM_COUNTRIES_BY_CODE.'<br>'; print_r($param); print_r($response); echo '</pre>';
	$countryOptions[] = "Select";
	foreach($response as $country){
		$countryOptions[$country['countryCode']] = ucwords(strtolower($country['countryName']));
	}
	return $countryOptions;
}


/**
 * Get Handset manufacturer List
 */
function get_mobile_handset_manufacturer() {
	global $config;
	$hmOptions = array();
	$param = array(
		"sitecode" => $config['sitecode'],
		"brand" => $config['brand']
	);	
	$response = apiPostNew(VM_HANDSET_MANUFACTURERS, $param);
	//echo '<pre> URL : '.VM_HANDSET_MANUFACTURERS.'<br>'; print_r($param); print_r($response); echo '</pre>';
	$hmOptions[] = t('Select');
	foreach($response as $key){
		$hmOptions[$key['mobilecode']] = $key['mobilename'];
	}
	return $hmOptions;
}


/**
 * Get VM_MOBILE_MODELS List
*/
function get_mobile_model_list($selected_hm) {
	global $config;
	$mOptions = array();
	if(!empty($selected_hm)){
		$param = array(
			"projectCode" => $config['product'],
			"languageCode" => 'ENG',
			"Mobilecode" => $selected_hm,
			"sitecode" => $config['sitecode'],
			"brand" => $config['brand']
		);	
		$response = apiPostNew(VM_MOBILE_MODELS, $param);
		//echo '<pre> URL : '.VM_MOBILE_MODELS.'<br>'; print_r($param); print_r($response); echo '</pre>';exit;
		$mOptions[] = t('Select');
		foreach($response as $key){
			$mOptions[$key['MobileID']] = $key['mobilemodel'];
		}
	}
	return $mOptions;
}



/**
 * Get VM_MOBILE_MODEL_DISCURSION
*/
function get_mobile_model_discursion($selected_m) {
	global $config;
	$varResult = '';
	if(!empty($selected_m)){
		$param = array(
			"projectCode" => $config['product'],
			"languageCode" => 'ENG',
			"MobileID" => $selected_m,
			"sitecode" => $config['sitecode'],
			"brand" => $config['brand']
		);	
		$response = apiPostNew(VM_MOBILE_MODEL_DISCURSION, $param);
		//echo '<pre> URL : '.VM_MOBILE_MODEL_DISCURSION.'<br>'; print_r($param); print_r($response); echo '</pre>';exit;

		if($response[0]['errcode'] == -1){ 
			$varResult = '<div class="col-sm-12">No Records Found.</div>';
		}else{
			$discursions = $response[0]['discursion'];
			$discursionsArr = explode("|", $discursions);
			foreach($discursionsArr as $ds){
				$varResult .= '<div class="col-sm-6">'. $ds .'</div>';
			}			
		}
	}
	return $varResult;
}
