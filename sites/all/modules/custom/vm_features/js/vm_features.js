(function ($) {
	$(document).ready(function(){

		//VM Features - Mobile Operators
		$( "#dropdown_dst_country_list" ).change(function(){

				var ccode = $(this).val();
				$('#div-operators-list').remove();
				$.ajax({
					url: Drupal.settings.basePath+"vmfeatures/operators/result",
					type: "POST",
					data: {
						country_code: ccode
					},
					success: function(data){
						if(data) { 
							$('.dst_country_list').append('<div id="div-operators-list">'+data+'</div>');
						} 
					}
				});
		});			
		
		//VM Features - Mobile Internet : Upon selection of Manufacturer, need to reset the Duscursion Markup.
		$( "#edit-dropdown-handset-manufacturer" ).change(function(){
			$('div[id^="edit-mob-discursion"]').html("");
		});
				
		$("input:radio[name=no_of_sims]").click(function(){
		
			$('div[id^="sim-container"]').hide();
			var no_of_sims = $(this).val();
			for(var i = 1; i <= no_of_sims; i++){
				$("#sim-container-" + i).show();
				$("#mobile-number-" + i).prop('required', true);
				$("#sim-number-" + i).prop('required', true);
			}
 			if(no_of_sims < 5){
				var next_item = no_of_sims++;
				for(var j = next_item + 1; j <= 5; j++){
					$("#mobile-number-" + j).removeClass('required');
					$("#sim-number-" + j).removeClass('required');
				}				
			}  
		});
		
	});
})(jQuery);