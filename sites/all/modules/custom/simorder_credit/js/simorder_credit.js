(function ($) {
	$(document).ready(function(){

	

		$("#simorder-with-credit-payment-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$(this).removeClass('error');
		}); 

		$(".fsc_billing_address_chkbox").on('click', function(){
			if($(".fsc_billing_address_chkbox").is(':checked')){
				$.ajax({
					url: Drupal.settings.basePath + 'simorder/credit/getdeliveryaddress',
					type: "POST",
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$("#address1").val( data.address1 );
						$("#address2").val( data.address2 );
						$("#town").val( data.town );
						$("#varchar_postcode").val( data.postcode );
						
						$(".billing-info .form-control").removeClass('error').parent().removeClass('has-error error-processed');
						$(".billing-info div.errormsg").hide();
						$(".billing-info span.error").hide();
					}
				});
			}else{
				$("#address1").val('');
				$("#address2").val('');
				$("#town").val('');
				$("#varchar_postcode").val('');
			}
		});

	});
})(jQuery);