(function ($) {
$(document).ready(function(){
	
 // Overrides the default auto complete filter function to search only from the beginning of the string
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };	
	 
	
	var availableTags = [
	'Afghanistan','Albania','Algeria','American Samoa','Andorra','Angola','Anguilla','Antarctica','Antigua Barbuda','Argentina','Armenia','Aruba','Ascension','Australia','Austria','Azerbaijan','Bahamas','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia','Bosnia Herzegovina','Botswana','Brazil','British Virgin Islands','Bulgaria','Burkina Faso','Burundi','Cambodia','Cameroon','Canada','Canary Islands','Cape Verde','Cayman Islands','Central African Republic','Chad','Chattham Islands','Chile','China','Christmas Islands','Cocos Islands','Colombia','Comoros','Congo','Congo CDR','Cook Islands','Costa Rica','Croatia','Cuba','Cyprus','Cyprus North','Czech Republic','Denmark','Diego Garcia','Djibouti','Dominica','Dominican Republic','East Timor','Ecuador','Egypt','ElSalvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia','Falkland Islands','Faroe Islands','Fiji','Finland','France','French Guiana','French Polynesia','Gabon','Gambia','Georgia','Germany','Ghana','Gibraltar','Greece','Greenland','Grenada','Guadeloupe','Guam','Guantanamo','Guatemala','Guinea Bissau','Guinea Republic','Guyana','Haiti','Honduras','Hong Kong','Hungary','Iceland','India','Indonesia','Iran','Iraq','Ireland','Israel','Italy','Ivory Coast','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea North','Korea South','Kosovo','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein','Lithuania','Luxembourg','Macau','Macedonia','Madagascar','Malawi','Malaysia','Maldives','Mali','Malta','Mariana Islands','Marshall Islands','Martinique','Mauritania','Mauritius','Mayotte','Mexico','Micronesia','Moldova','Monaco','Mongolia','Montenegro','Montserrat','Morocco','Mozambique','Myanmar Burma','Namibia','Nauru','Negara Brunei','Nepal','Netherlands','Netherlands Antilles','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island','Norway','Oman','Pakistan','Palau','Palestine','Panama','Papua New Guinea','Paraguay','Peru','Philippines','Poland','Portugal','Puerto Rico','Qatar','Reunion','Romania','Russia','Rwanda','Saipan Northern Marianas','San Marino','Sao Tome Principe','Saudi Arabia','Senegal','Senegal Sentel','Serbia','Seychelles','SierraLeone','Singapore','Slovakia','Slovenia','Slovenia IPKO','Solomon Islands','Somalia','South Sudan','South Africa','Spain','Sri Lanka','St Helena','St Kitts Nevis','St Lucia','St Martin','St Pierre Miquelon','St Vincent Grenadines','Sudan','Suriname','Swaziland','Sweden','Switzerland','Syria','Taiwan','Tajikistan','Tanzania','Thailand','Thuraya','Togo','Tokelau','Tonga','Trinidad and Tobago','Tunisia','Turkey','Turkmenistan','Turks Caicos','Tuvalu','Uganda','Ukraine','United Arab Emirates','United States','Uruguay','US Virgin Islands','Uzbekistan','Vanuatu','Vatican City','Venezuela','Vietnam','Wallis Futuna','Western Samoa','Yemen','Zambia','Zimbabwe','United Kingdom','Kenya-Celtel','Algeria Wataniya'];
    
	$( "#country_rates_international" ).autocomplete({
		source: availableTags, 
		autoFocus: true,
		minLength: 0,
		select: function (event, ui) {
			var icname = ui.item.value;
			$('.ctry li').removeAttr('class');
			$('img[alt="'+icname+'"]').closest('li').addClass('active').siblings().addClass('inactive');
			$.ajax({
				url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/international_result",
				type: "POST",
				data: {
					country_name: icname
				},
				success: function(data){
					if(data) { 
						$('#div-international-rates').remove();
						$('#home-international-calls').after('<div id="div-international-rates" class="table-wrapper">'+data+'</div>');
					} 
				}
			});
		}
	}).focus(function(){
            $(this).autocomplete("search", "");
     });
    
	
		 
	 $('.home-best-rates .ctry li img').on('click',function(){
		    var icname_flag = $(this).attr('alt');
			$('.ctry li').removeAttr('class');
			$(this).closest('li').addClass('active').siblings().addClass('inactive');
			$('#country_rates_international').val(icname_flag);
			 $.ajax({
						url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/international_result",
						type: "POST",
						data: {
							country_name: icname_flag
						},
						success: function(data){
							//alert(data);
							if(data) { 
								$('#div-international-rates').remove();
								$('#home-international-calls').after('<div id="div-international-rates" class="table-wrapper">'+data+'</div>');
							} 
						}
					});	
	 }); 
	
	
	
	
	
	$( "#country_rates_international_innerpage").autocomplete({
		source: availableTags, 
		autoFocus: true,
		minLength: 0,
		select: function (event, ui) {
			var icname = ui.item.value;
			//var icname_trim = ui.item.value.split(" ").join("");
			$('.ctry li').removeAttr('class');
			$('img[alt="'+icname+'"]').closest('li').addClass('active').siblings().addClass('inactive');
			$.ajax({
				url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/international_innerpage_result",
				type: "POST",
				data: {
					country_name: icname
				},
				success: function(data){
					//alert(data);
					if(data) {
						$('#div-international-rates').remove();					
						$('#international-calls').after('<div id="div-international-rates">'+data+'</div>');
					} 
				}
			});
		}
	}).focus(
	
			function(){
            $(this).autocomplete("search", "");
	});
	 
	 
	 
	 $('.international-rates .ctry li img').on('click',function(){
		    var icname_flag = $(this).attr('alt');
			$('.ctry li').removeAttr('class');
			$(this).closest('li').addClass('active').siblings().addClass('inactive');
			$('#country_rates_international_innerpage').val(icname_flag);
			 $.ajax({
						url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/international_innerpage_result",
						type: "POST",
						data: {
							country_name: icname_flag
						},
						success: function(data){
							//alert(data);
							if(data) { 
								$('#div-international-rates').remove();
								$('#international-calls').after('<div id="div-international-rates">'+data+'</div>');
							} 
						}
					});	
	 }); 
	 
	 
	 
	 
	 


	$( "#country_smart_international_rates" ).autocomplete({
		source: availableTags, 
		autoFocus: true,
		minLength: 0,
		select: function (event, ui) {
			var icname = ui.item.value;
			$.ajax({
				url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/international_smartrates_result",
				type: "POST",
				data: {
					country_name: icname
				},
				success: function(data){
					//alert(data);
					if(data) { 
						$('#div-international-rates').remove();
						$('.roaming-rates').after('<div id="div-international-rates">'+data+'</div>');
					} 
				}
			});
		}
	}).focus(function(){
            $(this).autocomplete("search", "");
     });
	
	$( "#country_rates_roaming_src" ).autocomplete({
		source: availableTags, 
		autoFocus: true,
		minLength: 0,
		select: function (event, ui) {
			
			var src_cname = ui.item.value;
			if($( "#country_rates_roaming_dst" ).val() == ''){		
				$("#country_rates_roaming_dst").show();
				$( "#country_rates_roaming_dst" ).val( $("#hdn_country_rates_roaming_dst").val() );
			}

				$.ajax({
					url: Drupal.settings.basePath+"rates/roaming_result",
					type: "POST",
					data: {
						src_country_name: src_cname,
						dst_country_name: $( "#country_rates_roaming_dst" ).val()
					},
					success: function(data){
						if(data) { 
							var spltresult = data.split('~');
							$('#div-roaming-rates').remove();
							$('#div-roaming-rates1').remove();
							$('#roaming-result-from').after('<div id="div-roaming-rates" class="roaming-result text-voilet">'+spltresult[0]+'</div>');
							$('#roaming-result-to').after('<div id="div-roaming-rates1" class="roaming-result text-blue">'+spltresult[1]+'</div>');
						} 
					}
				});
			// }
		}
	}).focus(function(){
            $(this).autocomplete("search", "");
     });

	// Vectone App Rates

	  $( "#country_rates_vectoneapp" ).autocomplete({
			source: availableTags, 
			autoFocus: true,
			minLength: 0,
			select: function (event, ui) {
				var icname = ui.item.value;
				$('#div-vectoneapp-rates').remove();
				$.ajax({
					url: Drupal.settings.basePath+"rates/vectoneapp_result",
					type: "POST",
					data: {
						country_name: icname
					},
					success: function(data){
						if(data) { 
							$('#international-app-rates').after('<div id="div-vectoneapp-rates" class="roaming-result text-blue">'+data+'</div>');
						} 
					}
				});
			}
		}).focus(function(){
				$(this).autocomplete("search", "");
		 });  	
	 
	 
	// Roaming Rates 
	$( "#country_rates_roaming_dst" ).autocomplete({
		source: availableTags, 
		autoFocus: true,
		minLength: 0,
		select: function (event, ui) {
			var dst_cname = ui.item.value;
			if($( "#country_rates_roaming_src" ).val() != ''){
				$.ajax({
					url: Drupal.settings.basePath + Drupal.settings.pathPrefix + "rates/roaming_result",
					type: "POST",
					data: {
						src_country_name: $( "#country_rates_roaming_src" ).val(),
						dst_country_name: dst_cname
					},
					success: function(data){
						if(data) { 
							var spltresult = data.split('~');
							$('#div-roaming-rates').remove();
							$('#div-roaming-rates1').remove();
							$('#roaming-result-from').after('<div id="div-roaming-rates" class="roaming-result text-voilet">'+spltresult[0]+'</div>');
							$('#roaming-result-to').after('<div id="div-roaming-rates1" class="roaming-result text-blue">'+spltresult[1]+'</div>');
						} 
					}
				});
			}
		}
	}).focus(function(){
            $(this).autocomplete("search", "");
     });	
	
});
})(jQuery);