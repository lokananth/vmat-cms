(function ($) {
	$(document).ready(function(){

		$("#sendasim-abroad-address-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$(this).removeClass('error');			
		}); 
		
		$(".fsa_billing_address_chkbox").on('click', function(){
			if($(".fsa_billing_address_chkbox").is(':checked')){
				$(".loader-bg").show();
				$.ajax({
					url: Drupal.settings.basePath + 'sendasim/abroad/getdeliveryaddress',
					type: "POST",
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$(".loader-bg").hide();
						$(".bill_houseno").val( data.houseno );
						$(".bill_street").val( data.street );
						$(".bill_town").val( data.town );
						$(".varchar_postcode").val( data.postcode );
						
						$(".billing-info .form-control").removeClass('error').parent().removeClass('has-error error-processed');
						$(".billing-info div.errormsg").hide();
						$(".billing-info span.error").hide();
					}
				});
			}else{
				$(".bill_houseno").val('');
				$(".bill_street").val('');
				$(".bill_town").val('');
				$(".varchar_postcode").val('');
			}
		});
		
		$(".dst_country_list").on('change', function(){
			var dst_country = $(this).val();
			$(".span_sim_cost").text('');
			$(".span_delivery_cost").text('');
			$('input[name="delivery_cost"]').val('');
			if(dst_country != ''){
				$(".loader-bg").show();
				$.ajax({
					url: Drupal.settings.basePath + 'sendasim/abroad/getdeliverycost',
					type: "POST",
					data:{
						dst_country: dst_country
					},
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$(".loader-bg").hide();
						if(data[0].errcode == 0){
							$(".span_sim_cost").text('Free');
							$(".span_delivery_cost").text(data[0].Currency_Code + ' ' + data[0].Delivery_Cost );
							$('input[name="delivery_cost"]').val( data[0].Delivery_Cost );
						}
					}
				});
			}
		});

/*		
    $("#sendasim-abroad-address-form #email").on('blur' , function(){ 
        if($.trim($(this).val()) != ''){
            $('#divTEmail').remove();
			$(this).removeClass('error').parent().removeClass('has-error error-processed');
			var userinput = $('#email').val();
			var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

			if( !pattern.test(userinput) ) {
				$('#divTEmail').remove();
				$('#email').after('<div id="divTEmail" class="errormsg">'+ Drupal.t("Please enter an valid email address.") + '</div>');
				$(this).addClass('error').parent().addClass('has-error error-processed');
			}
        }else{
			$('#divTEmail').remove();
			$('#email').after('<div id="divTEmail" class="errormsg">'+ Drupal.t("Please enter an email address.") + '</div>');
			$(this).addClass('error').parent().addClass('has-error error-processed');
		}
    });
 	
	$("#sendasim-abroad-address-form #reg_title").on('blur' , function() { 
		$('#divTitle').remove();
		$(this).removeClass('error').parent().removeClass('has-error error-processed');
		if($.trim($(this).val()) == ''){
		$('#reg_title').after('<div id="divTitle" class="errormsg">'+ Drupal.t('Please choose the title.') + '</div>');
		$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	
	$("#sendasim-abroad-address-form #first_name").on('blur' , function() { 
		$('#divFName').remove();
		$(this).removeClass('error').parent().removeClass('has-error error-processed');
		if($.trim($(this).val()) == ''){
		$('#first_name').after('<div id="divFName" class="errormsg">'+ Drupal.t('Please enter your first name.') + '</div>');
		$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	

	$("#sendasim-abroad-address-form #last_name").on('blur' , function() { 
		$(this).removeClass('error').parent().removeClass('has-error error-processed');
		$('#divLName').remove();
		if($.trim($(this).val()) == ''){
		$('#last_name').after('<div id="divLName" class="errormsg">'+ Drupal.t('Please enter your last name.') + '</div>');
		$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	
	

	$("#sendasim-abroad-address-form #edit-houseno").on('blur' , function() { 
	$(this).removeClass('error').parent().removeClass('has-error error-processed');
		$('#divAdr2').remove();
		if($.trim($(this).val()) == ''){
			$('#edit-houseno').after('<div id="divAdr2" class="errormsg">'+ Drupal.t('Please enter the House no') + '</div>');
			$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});		
	$("#sendasim-abroad-address-form #street").on('blur' , function() { 
	$(this).removeClass('error').parent().removeClass('has-error error-processed');
		$('#divAdr3').remove();
		if($.trim($(this).val()) == ''){
			$('#street').after('<div id="divAdr3" class="errormsg">'+ Drupal.t('Please enter the Street Name') + '</div>');
			$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	
	$("#sendasim-abroad-address-form #town").on('blur' , function() { 
	 $(this).removeClass('error').parent().removeClass('has-error error-processed');
		$('#divAdr4').remove();
		if($.trim($(this).val()) == ''){
			$('#town').after('<div id="divAdr4" class="errormsg">'+ Drupal.t('Please enter the town.') + '</div>');
			$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	
	$("#sendasim-abroad-address-form #bill_pin_code").on('blur' , function() { 
	$(this).removeClass('error').parent().removeClass('has-error error-processed');
		$('#divAdr5').remove();
		if($.trim($(this).val()) == ''){
			$('#bill_pin_code').after('<div id="divAdr5" class="errormsg">'+ Drupal.t('Please enter the postcode.') + '</div>');
			$(this).addClass('error').parent().addClass('has-error error-processed');
		}
	});	
		
 */		
		
		
	});
})(jQuery);