(function($) {
    $(document).ready(function() {
		
        $("#vcode").on('click', function(e) {
            $.ajax({
                url: Drupal.settings.basePath + 'myaccount/register/pin_resend',
                type: "POST",
                success: function(data) {
                    window.location = data;
                }
            });
        });

       /*  //BEGIN: Register Step 1 - form validation
		$("#myaccount-registration-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});
        //BEGIN: Register Step 2 - form validation
		$("#myaccount-reg-verification-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});        
		//BEGIN: Register Step 3 - form validation
		$("#myaccount-reg-continue-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});	 */

		$("#myaccount-reg-continue-form #edit-password, #myaccount-reg-continue-form #edit-confirm-password").on('blur' , function(){ 
        if($.trim($("#edit-password").val()) != '' && $.trim($("#edit-confirm-password").val()) != ''){
            $('#divTCPWD').remove();
			var mobile = $('#edit-password').val();
			var mobile1 = $('#edit-confirm-password').val();
			if( mobile != mobile1 ) {
				$('#divTCPWD').remove();
				$('#edit-confirm-password').after('<div id="divTCPWD" class="errormsg">'+ Drupal.t("Password and Confirm Password did not match.") + '</div>');
			}
        }
		});
		
		$("#myaccount-reg-continue-form #edit-email, #myaccount-reg-continue-form #edit-confirm-email").on('blur' , function(){ 
        if($.trim($("#edit-email").val()) != '' && $.trim($("#edit-confirm-email").val()) != ''){
            $('#divTCEmail').remove();
			var mobile2 = $('#edit-email').val();
			var mobile21 = $('#edit-confirm-email').val();
			if( mobile2 != mobile21 ) {
				$('#divTCEmail').remove();
				$('#edit-confirm-email').after('<div id="divTCEmail" class="errormsg">'+ Drupal.t("Email and Confirm Email did not match.") + '</div>');
			}
        }
		});	
	/* 	
		//Login Form Validation
		$("#myaccount-login-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});
		
        // Forgot password form validation
		$("#myaccount-forgot-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
			$(".error").remove();
		});        
		
		// Reset password form validation
		$("#myaccount-reset-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		}); */		

    });
 
})(jQuery);