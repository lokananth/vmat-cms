(function($) { 
    $(document).ready(function() {

        $("#llom_vcode").on('click', function(e) {
            $.ajax({
                url: Drupal.settings.basePath + 'llom/pin_resend',
                type: "POST",
                success: function(data) {
                    window.location = data;
                }
            });
        });		
		
    });		
	
	$(document).on("click", ".llom_select_class", function(e){
		var get_id = this.id;		
		var split_id = get_id.split('~');
		var get_itemid = split_id[1];
		$('input[name="plan-type"]').val(get_itemid);
		$(".redirect-loader-bg").show();
		$('#llom-step1-form').submit();
	});
	
})(jQuery);
