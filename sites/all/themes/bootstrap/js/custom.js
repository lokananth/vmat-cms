(function ($) {
$(document).ready(function () {
	  $('.visa-c').on('click', function(){
	  
	    window.open('http://www.mastercard.co.uk/securecode.html','mywindowtitle','width=700,height=650');
	 
	  }); 
	  $('.master-c').on('click', function(){
	  
	    window.open('http://www.visaeurope.com/en/cardholders/verified_by_visa.aspx','mywindowtitle','width=700,height=650');
	 
	  });
    $('.panel').matchHeight();
	$('.panel-heading').matchHeight();
	$('.top-part').matchHeight();
	$('.bottom-part').matchHeight();
	
	$("#Pause").click(function () { 
		   $('#slider').data('nivoslider').stop(); 
	});        
	$("#Play").click(function () { 
	   $('#slider').data('nivoslider').start();
	});  
	
	
 	$('.form-submit').on('click', function(){
	    $('.required').each( function(){
			if($(this).val()==''){
				if ($('#security_code').length){
					$('#security_code').val('');
				}
			}
		});
	});	 
	
	$('#freesim-seo-registration-form .form-control ').on('keyup', function(){
         $(".alert").fadeOut();
         $('.cheap-calls-rate').height('auto');

	});
	
	$('.required').on('keyup change', function(){
		if($(this).val()!=''){
			$(this).removeClass('error').parent().removeClass('has-error error-processed');
			$(".alert").fadeOut();
		}		
	});

		
	$("#edit-submitted-email,#edit-submitted-e-post").attr("maxlength",60);  
	$("#edit-submitted-telefoon-nummer,#edit-submitted-contact-number").attr("maxlength",15);

	$('input.disablecopypaste').bind('copy paste', function (e) {
		   e.preventDefault();
	});

	$(".disablecopypaste,.disable_mnumber").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});	  
	$('.form-submit').on('click', function(){
	   $(this).closest('.panel').css('height','auto');
	})
	
	$('#edit-login-btn-submit').click( function (){
		if ($('#edit-condtion').is(':checked')) {
			var username = $('#username').val();
			var password = $('#edit-password').val();
			// set cookies to expire in 14 days
			$.cookie('username', username, { expires: 14 });
			$.cookie('password', password, { expires: 14 });
			$.cookie('remember', true, { expires: 14 });
		} else {
			// reset cookies
			$.cookie('username', null);
			$.cookie('password', null);
			$.cookie('remember', null);
		}
	});

	var remember = $.cookie('remember');
	if ( remember == 'true' ) {
		var username = $.cookie('username');
		var password = $.cookie('password');
		// autofill the fields
		$('#edit-condtion').attr("checked" , true );
		$('#username').val(username);
		$('#edit-password').val(password);
	}
	
	$.fn.restrictInputs = function(restrictPattern){
		var targets = $(this);
		var pattern = restrictPattern || /[^a-zA-Z0-9@._-]/g // some default pattern

		var restrictHandler = function(){
			var val = $(this).val();
			var newVal = val.replace(pattern, '');

			if (val !== newVal) { // To prevent selection and keyboard navigation issues
				$(this).val(newVal);
			}
		};

		targets.on('keyup', restrictHandler);
		targets.on('paste', restrictHandler);
		targets.on('change', restrictHandler);
	};

	$('#edit-email, #edit-confirm-email, #email').restrictInputs();
 
    $('#edit-password,#edit-confirm-password').keypress(function(key) {
        if(key.charCode == 32) return false;
    });
	
	$('#edit-password,#edit-confirm-password,#edit-confirm-email, #confirm-mobile-number, #edit-confmobile-number-inp').bind("paste",function(e) {
		e.preventDefault();
	});
 
	$('.visa-c').on('click', function(){
		window.open('http://www.visaeurope.com/en/cardholders/verified_by_visa.aspx','mywindowtitle','width=700,height=650');
	}); 
	
	$('.master-c').on('click', function(){
		window.open('http://www.mastercard.co.uk/securecode.html','mywindowtitle','width=700,height=650');
	});
	
	$(".edit-link").on("contextmenu",function(){
       return false;
    }); 

 	$('#card_number,#security_code').bind("cut copy paste",function(e) {
		e.preventDefault();
	}); 

	$.fn.scrollView = function () {
		return this.each(function () {  
			$('html, body').animate({
				scrollTop: $(this).parent().offset().top
			}, 'slow' );
		});
	}

	if ( $('#main-content').next().is('.alert-block')) {
		$('.alert-block').scrollView();
	} else {
		$('#topup-registration-form, #status-help, #topup-review-confirm, #topup-payment-form, #myaccount-login-form, #myaccount-registration-form, #myaccount-forgot-password-form, #freesim-registration-form, .status-help,.topup_payment, #topup-paysafe-step2-form,.other_payment, #paym-registration-form, #paym-number-validate-form, #paym-personal-details-form, #paym-direct-debit-enrollment-form, #paym-payment-form, #paym-payment-review, #myaccount-reg-verification-form, #myaccount-reg-continue-form, #topup-paysafe-review-confirm, #myaccount-reset-password-form, #llom-step1-form, #llom-step2-form, #llom-step3-form,#llom-step4-form, #llom-step5-form, #freesim-abroad-form, #freesim-abroad-address-form, #freesim-abroad-payment-form, #freesim-abroad-review-confirm, #sendasim-abroad-form, #sendasim-abroad-address-form, #sendasim-abroad-payment-form, #sendasim-abroad-review-confirm, #freesim-dashboard-form, #llom-pay-by-balance-review-form, #simorder-with-credit-form, #simorder-with-credit-payment-form, #simorder-with-credit-review-confirm, #vm-features-sim-activation-form').parent().parent().scrollView();
	}		
	
	//Allow Numbers Only
	$('#security_code, #pin_code, #edit-captcha-response, #edit-contact-number, #edit-submitted-contact-number, #edit-submitted-telefoon-nummer, #edit-issue-no, #edit-verification-code, #username, #edit-verification-code, #edit-contact-number, #mobile-number, #card_number, #edit-pin-code, #contact_number, #edit-mobile-number, #confirm-mobile-number, .houseno, .bill_houseno, #edit-mobile-number-otp,#edit-postcode, #edit-mobile-number-inp,#edit-confmobile-number-inp, #edit-main-vectone-number-inp, #edit-sharer-vectone-number-inp, #edit-sharer-vectone-number2-inp, #edit-sharer-vectone-number3-inp').bind('keyup blur', function(e){ 
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
        }		
	});
	
	//Allow Alphabets only
	$("#cardholder_name, #name, #town, #edit-first-name, #edit-last-name, #edit-name, #edit-submitted-first-name, #edit-submitted-last-name, #edit-town").bind('keyup blur', function(e){		
		/*var node = $(this);
		node.next('.errormsg').empty();
		if (String.fromCharCode(e.which).match(/[^a-z A-Z]+/)) {
			return false;
		}*/
		if (this.value.match(/[^a-z A-Z]+/g)) {
			this.value = this.value.replace(/[^a-z A-Z]+/g, '');
        }			
	});

	//Allow Alphanumeric with comma, fullstop & space
	$('#address1, #address2, #varchar_postcode').bind('keyup blur', function(e){ 
		if (this.value.match(/[^a-z A-Z0-9,.]+/g)) {
			this.value = this.value.replace(/[^a-z A-Z0-9,.]+/g, '');
        }		
	});	
	//Payment Form Expiry date validation message on blur option
	$("#edit-expiry-title-month, #edit-expiry-title-year,#edit-dob-title-day, #edit-dob-title-month, #edit-dob-title-year").on('change blur' , function() { 
		$('#divExpDate').remove();
		$('#divdob').remove();
		$(this).removeClass('error').parent().removeClass('has-error error-processed');
		if($.trim($(this).val()) == ''){
			$('#edit-expiry-title').after('<div id="divExpDate" class="errormsg">'+ Drupal.t('Please select Expiry Month & Year.') + '</div>');
			$('#edit-dob-title').after('<div id="divdob" class="errormsg">'+ Drupal.t('Please select Date of birth') + '</div>');
			$(this).addClass('error');
		}
	});

	$('.form-submit').on('click', function(){
       $('#divExpDate').remove();
	   $('#divdob').remove();
	   $('#divterms').remove();
		if($('#edit-expiry-title-month, #edit-expiry-title-year').val()==''){
			$('#edit-expiry-title').after('<div id="divExpDate" class="errormsg">'+ Drupal.t('Please select Expiry Month & Year.') + '</div>');
			$('#edit-expiry-title-month, #edit-expiry-title-year').addClass('error');
		}
		if($('#edit-dob-title-day, #edit-dob-title-month, #edit-dob-title-year').val()==''){
			$('#edit-dob-title').after('<div id="divdob" class="errormsg">'+ Drupal.t('Please select Date of birth') + '</div>');
			$('#edit-dob-title-day, #edit-dob-title-month, #edit-dob-title-year').addClass('error');
		}
		if($('#condition,#pre_condition,#edit-condition').is(':checked')==false){
			$(".form-item-condition label").after('<div id="divterms" class="errormsg">'+ Drupal.t('Please accept our terms & conditions') + '</div>');
			$(".form-item-condition span.error").hide();
		}
	});	
	
	$('#condition,#pre_condition,#edit-condition').on('change', function(){
	    $(".form-item-condition span.error").hide();
		 if($(this).is(':checked')==true){
			$('#divterms').remove();
		 }
	})
	

	
	$("#email, #edit-email, #confirm_email").on('blur keyup' , function(){ 
		 $(this).next('.errormsg').hide();
		 if($.trim($(this).val()) != ''){
			var userinput = $(this).val();
			var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
			if( !pattern.test(userinput) ) {
				$(this).after("<span class='errormsg'>"+ Drupal.t('Please enter an valid email address.') + "</span>");
				//$('#email+span.error, #edit-email+span.error, #edit-confirm-email+span.error').hide();
				$(this).next('.error').hide();
		}
	}
    });       

	
	
	//Email validation
	$("#email").on('blur keyup' , function(){ 
         if($.trim($(this).val()) != ''){
			var userinput = $('#email').val();
			var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
            $('#email+span.errormsg').hide();
			if( !pattern.test(userinput) ) {
				$(this).after("<span class='errormsg'>"+ Drupal.t('Please enter an valid email address.') + "</span>");
				$('#email+span.error').hide();
			}
        }
    });
	
	
	//Load External Loader on Custom Ajax calls & System Ajax Callbacks.
	$(document).ajaxSend(function(e,x,settings){
		$('.errormsg').hide(); //Hiding onblur error messages while clicking Form Submit Ajax Button.
		$(".loader-bg").show();
	}).ajaxComplete(function(e,x,settings){
		$(".loader-bg").hide();
		     //$('#security_code').val('');
		 if($('.alert-danger').is(':visible') == true){
			 $('.cheap-calls-rate').height('auto');
			 var hgt = $('.alert-danger').height();
			 var hgt1 = $('.cheap-calls-rate').height();
		      var setheight = hgt + hgt1 + 50;
			  $('.cheap-calls-rate').height(setheight);
			  
	   } 
	});
	
		$('#seo-sim-section .form-submit').on('click', function() {
						  $('.cheap-calls-rate').height('auto');
						  var hgt = $('span.error').length;
						  var hgt1 = $('span.error').height();
						  var hgt2 = $('.cheap-calls-rate').height();
						  var setheight1 = hgt*hgt1;
						  var setheight = setheight1 + hgt2 + 30;
						  $('.cheap-calls-rate').height(setheight);
						

	   });	
		
		$('#seo-sim-section .form-control').on('keyup blur', function() {
		$('.cheap-calls-rate').height('auto'); 
	  if($('span.error').is(':visible') == true){
						  $('.alert-danger').fadeOut();
						  var hgt = $('span.error').length;
						  var hgt1 = $('span.error').height();
						  var hgt2 = $('.cheap-calls-rate').height();
						  var setheight1 = hgt*hgt1;
						  var setheight = setheight1 + hgt2 + 30;
						  $('.cheap-calls-rate').height(setheight);
									  
	  }
	   });
				
	$( window ).resize(function() {
	 if($('.error').is(':visible') == true){
	   $('.cheap-calls-rate').height('auto');
			var hgt = $('span.error').length;
			var hgt1 = $('span.error').height();
			var hgt2 = $('.cheap-calls-rate').height();
			var setheight1 = hgt*hgt1;
			var setheight = setheight1 + hgt2 + 30;
		  $('.cheap-calls-rate').height(setheight);
	   }
	});
	$('#simorder-with-credit-form .btn-grey-dark').on('click',function(){
	 $(this).scrollView();
	})
	 
});
})(jQuery);

(function($) {
    Drupal.ajax.prototype.commands.customAjaxRedirect = function(ajax, response, status) {
		$(".redirect-loader-bg").show();
        window.location = response.url;
    }

})(jQuery);
