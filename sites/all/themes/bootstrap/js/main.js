jQuery(function($) {  
	//Initiat WOW JS
	new WOW().init();
     
});


(function ($) {
    $('#homeCarousel').carousel({
        interval: 2000,
        pause: "false"
    });
    $('#playButton').click(function () {
        $('#homeCarousel').carousel('cycle');
    });
    $('#pauseButton').click(function () {
        $('#homeCarousel').carousel('pause');
    });



$("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

})(jQuery);